A discord color scheme inspired by [One Dark](https://github.com/atom/atom/tree/master/packages/one-dark-syntax).

# Preview:
![](example-screenshot.png)

# Installation:
Clone this repository or download the `onedark.css` file.

After that, follow the instructions on the [Beautiful Discord](https://github.com/leovoel/BeautifulDiscord) page.

# Discord
Join [this discord guild](https://discord.gg/EdwmUsJ) for real time help and notifications about updates.

# Credits:
## Fonts:
- [Fira Code](https://github.com/tonsky/FiraCode) (Code blocks)
- [Fira Mono](https://github.com/mozilla/Fira) (Inline code and the text field)

## Color Scheme:
- [One Dark](https://github.com/atom/atom/tree/master/packages/one-dark-syntax)
- [highlight.js](https://github.com/highlightjs/highlight.js/blob/master/src/styles/atom-one-dark.css)

